package net.goout
import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@ComponentScan(basePackages = ["org.springdoc"])
open class OpenApiConfig() : WebMvcConfigurer {

    init {
        // For details, see https://springdoc.org/#swagger-ui-properties
        // Controls the display of extensions (pattern, maxLength, minLength, maximum, minimum) fields and values for Parameters.
        System.setProperty("springdoc.swagger-ui.showCommonExtensions", "true")
    }

    // Defined in Jackson.kt, and contains the lowercase module.
    @Bean
    open fun objectMapper(): ObjectMapper = jackson

    @Bean
    open fun goOutOpenApi(): OpenAPI = OpenAPI().info(Info().title("API documentation"))
}
