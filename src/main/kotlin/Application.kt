package net.goout

import java.util.Arrays
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@EnableWebMvc
@Import(
        TestController::class,
        WebConfigurer::class,
        OpenApiConfig::class,
)
@Configuration
open class AppConfig

open class DispatcherServletInitializer : AbstractAnnotationConfigDispatcherServletInitializer() {
    override fun getServletMappings(): Array<String> = arrayOf("/*")

    override fun getServletConfigClasses(): Array<Class<*>>? = null

    override fun getRootConfigClasses(): Array<Class<*>> = arrayOf(AppConfig::class.java)
}

@Configuration
open class WebConfigurer : WebMvcConfigurer {
    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        // to use special character in path variables, for example, `email@dom.com`
        configurer.setUseSuffixPatternMatch(false)
    }

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        // to avoid HttpMediaTypeNotAcceptableException on standalone tomcat
        configurer.favorPathExtension(false)
    }

    // Note that @EnableWebMvc prevents using of @Primary @Bean ObjectMapper, we need to
    // replace it as follows (taken from https://stackoverflow.com/questions/45734108)
    override fun extendMessageConverters(converters: List<HttpMessageConverter<*>>) {
        converters
                .mapNotNull { converter -> converter as? MappingJackson2HttpMessageConverter }
                .forEach { jacksonConverter -> jacksonConverter.objectMapper = jackson }
    }
}

@SpringBootApplication
open class Application {}

fun main(args: Array<String>) {
    val ctx = SpringApplication.run(Application::class.java, *args)
    println("Let's inspect the beans provided by Spring Boot:")
    val beanNames: Array<String> = ctx.getBeanDefinitionNames()
    java.util.Arrays.sort(beanNames)
    for (beanName in beanNames) {
        println(beanName)
    }
}
