package net.goout

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.util.Random

@RestController
open class TestController() {
    @GetMapping("/test")
    fun get() = TestDto(TestEnum.values()[Random().nextInt(TestEnum.values().size)])
}

data class TestDto(
    val enum: TestEnum
)

enum class TestEnum {
    FIRST,
    SECOND,
    THIRD,
}
