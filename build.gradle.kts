import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    extra["spring.version"] = "5.3.1"
    extra["jvm.version"] = "15"
    extra["kotlin.version"] = "1.5.30-RC"
    extra["kotlinx.version"] = "1.5.1"
    extra["junit.version"] = "5.7.2"
    extra["log4j.version"] = "2.14.0"
    extra["jackson.version"] = "2.12.1"
}

plugins {
    val kotlinVersion = "1.5.30-RC"
    base
    kotlin("jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.springframework.boot") version "2.5.0"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    war
}

// To setup IntelliJ Idea for local deployment (with exploded artifacts), see https://stackoverflow.com/a/49523230/922584.
// Output war must be named ROOT to be mapped to / by Tomcat within CI.
tasks.war { archiveFileName.set("ROOT.war") }
// Register named war for local development so IntelliJ Idea correctly creates deployable artifacts.
tasks.register<War>(project.name)

allprojects {
    group = "net.goout"
    apply(plugin = "kotlin")

    repositories {
        mavenCentral()
        maven(url = "https://jitpack.io")
        maven(url = "https://plugins.gradle.org/m2/")

        if (project.hasProperty("ci")) {
            maven(url = "${project.property("ci")}")
        }
    }

    dependencies {
        api(kotlin("stdlib"))
        implementation("org.springframework.boot:spring-boot-starter-web")
        implementation("org.springframework:spring-context:${rootProject.extra["spring.version"]}")
        implementation("org.springframework:spring-webmvc:${rootProject.extra["spring.version"]}")
        implementation("org.springframework:spring-context-support:${rootProject.extra["spring.version"]}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${rootProject.extra["kotlin.version"]}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib:${rootProject.extra["kotlin.version"]}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${rootProject.extra["kotlin.version"]}")

        // OpenApi doc generator.
        implementation("org.springdoc:springdoc-openapi-ui:1.5.10")
        implementation("org.springdoc:springdoc-openapi-kotlin:1.5.10")

        // JDK 11 requirement for servlet API
        // https://mvnrepository.com/artifact/javax.servlet/servlet-api
        implementation("javax.servlet:javax.servlet-api:4.0.1")
        implementation("javax.xml.bind:jaxb-api:2.3.0")
        implementation("com.sun.xml.bind:jaxb-core:2.3.0")
        implementation("com.sun.xml.bind:jaxb-impl:2.3.0")
    }

    kotlin {
        jvmToolchain {
            (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of(rootProject.extra["jvm.version"].toString()))
        }
    }

    tasks.compileKotlin {
        kotlinOptions.jvmTarget = rootProject.extra["jvm.version"].toString()
    }

    tasks.compileTestKotlin {
        kotlinOptions.jvmTarget = rootProject.extra["jvm.version"].toString()
    }
}
