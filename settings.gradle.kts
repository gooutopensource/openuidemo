rootProject.name = "Backend"

fileTree(rootProject.projectDir)
    .matching {
        // include all subprojects
        include("**/build.gradle.kts")
        // exclude the root build file
        exclude("build.gradle.kts")
    }
    .toCollection(mutableListOf())
    .map { project ->
        // gradle uses its own special path separator
        relativePath(project.parent).replace(File.separator, ":")
    }
    .forEach { include(it) }
